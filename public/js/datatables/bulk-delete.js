/**
 * Starts the bulk delete process.
 *
 * @param {string} csrf_token - The CSRF token for authentication.
 * @param {string} bulkDeleteUrl - The URL for the bulk delete endpoint.
 * @param {string} table - The selector for the table to perform bulk delete on.
 */
const startBulkDelete = (csrf_token, bulkDeleteUrl, table) => {
    const deleteSelectedForm = document.querySelector('#deleteSelectedForm');
    const ids = document.querySelector('#deleteSelectedForm input#ids');

    // let selectedRowsArr = []
    // let selectedRows = document.querySelectorAll('tr.selected');

    let selectedRowsArr = [];
    let related_table = document.querySelector(table);
    let selectedRows = related_table.querySelectorAll('input[type="checkbox"]:checked');

    selectedRows.forEach(row => {
        selectedRowsArr.push(row.id.split('-')[1]);
    });

    selectedRowsArr = selectedRowsArr.filter(row => row !== undefined);

    console.log(selectedRowsArr);

    if (selectedRowsArr.length == 0) {
        Swal.fire({
          title: 'Error',
          text: 'Please select minimal 1 data!',
          icon: 'error',
        });
    } else {
        Swal.fire({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover your datas!",
            icon: "warning",
            showCancelButton: true,
        }).then(function (result) {
              if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: bulkDeleteUrl,
                    data: {
                        '_method': 'DELETE',
                        'ids': selectedRowsArr.toString()
                    },
                    headers: {
                        'X-CSRF-TOKEN': csrf_token
                    },
                    success: (response) => {
                        console.log(response);

                        $(table).dataTable().api().draw();

                        Swal.fire({
                            icon: 'success',
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                        });
                    }
                });
              }
            });
    }
}
