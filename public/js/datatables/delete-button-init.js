const deleteButtonInit = (csrf_token, table) => {
    const deleteButtons = document.querySelectorAll('#deleteButton');
    deleteButtons.forEach(button => {
        button.addEventListener('click', (e) => {
            Swal.fire({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                showCancelButton: true,
                showConfirmButton: true,
                confirmButtonText: 'Yes, delete!'
            })
                .then((result) => {
                  console.log(result);
                    if (result.isConfirmed) {
                        let modelId = e.target.dataset.modelId;
                        let deleteUrl = `${location.href}/${modelId}`;

                        $.ajax({
                            type: 'post',
                            url: deleteUrl,
                            data: {
                                '_method': 'DELETE'
                            },
                            headers: {
                                'X-CSRF-TOKEN': csrf_token
                            },
                            success: () => {
                                $(table).dataTable().api().draw();

                                Swal.fire({
                                    icon: 'success',
                                    title: 'Deleted!',
                                    text: 'Your file has been deleted.',
                                });
                            }
                        })
                    }
                });
        })
    });
}
