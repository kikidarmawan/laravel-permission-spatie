<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Implicitly grant "Super Admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('super admin') ? true : null;
        });

        if (!$this->app->routesAreCached()) {
            Passport::routes();

            Passport::tokensCan([
                "admin-access" => '',
                "blogs-create" => '',
                "blogs-delete" => '',
                "blogs-read" => '',
                "blogs-update" => '',
                "contents-control" => '',
                "permissions-create" => '',
                "permissions-delete" => '',
                "permissions-read" => '',
                "permissions-update" => '',
                "roles-create" => '',
                "roles-delete" => '',
                "roles-read" => '',
                "roles-update" => '',
                "systems-control" => '',
                "users-create" => '',
                "users-delete" => '',
                "users-read" => '',
                "users-update" => ''
            ]);

            Passport::tokensExpireIn(now()->addDays(15));
            Passport::refreshTokensExpireIn(now()->addDays(30));
            Passport::personalAccessTokensExpireIn(now()->addMonths(6));
        }
    }
}
