<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Spatie\Permission\Exceptions\UnauthorizedException;

class ApiCanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $permission_guard = null)
    {
        if (!$permission_guard) {
            return response()->json([
                'message' => 'Permission is not defined. try api_can:permissionName=guardName. Remember, the guard name is optional'
            ], 401);
        }

        if (auth('api')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $exploded = explode('=', $permission_guard);

        if (!$exploded) {
            return response()->json(123);
        }

        $permission = $exploded[0];
        $guard = $exploded[1] ?? 'web';


        $user = auth('api')->user();

        if ($user->hasPermissionTo($permission, $guard)) {
            return $next($request);
        }

        throw UnauthorizedException::forPermissions([$permission]);
    }
}
