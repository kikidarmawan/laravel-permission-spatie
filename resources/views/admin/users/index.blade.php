@extends('layouts.main', [
    'title' => 'Users',
])

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">

    <style>
        .select2-search__field::placeholder {
            color: #6e6b7b;
            opacity: 0.6;
            padding: 0 0.5rem;
        }
    </style>
@endpush

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Dashboard</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Dashboard
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="">Users</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            {{--  --}}
        </div>
    </div>

    <div class="content-body">
        <section id="basic-example">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <table class="datatables-basic table" id="datatable-users">
                            <thead>
                                <tr>
                                    <th style="cursor: pointer" id="selector">
                                        <input type="checkbox" class="w-100" style="cursor: pointer">
                                        <span style="display: none;">Selector</span>
                                    </th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roles</th>
                                    <th>Created At</td>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- START: Modal Create Users --}}
    <div class="modal fade text-start" id="modal-create-users" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Create Users</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form action="{{ route('admin.users.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-2">
                            <label>Name: </label>
                            <div class="mb-1">
                                <input type="text" id="name" name="name" placeholder="Users Name" class="form-control" />
                            </div>

                            @error('name')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>

                        <div class="mb-2">
                            <label>Email: </label>
                            <div class="mb-1">
                                <input type="email" id="email" name="email" placeholder="example@gmail.com" class="form-control" />
                            </div>

                            @error('email')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>

                        <div class="mb-2">
                            <label>Roles: </label>
                            <div class="mb-1">
                                <select class="select2 form-select" name="roles" style="width: 100%;">
                                    <option></option>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->name }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>

                                @error('roles')
                                    <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-2">
                            <label>Password: </label>
                            <div class="mb-1">
                                <input type="text" id="password" name="password" placeholder="Users Password" class="form-control" />
                            </div>

                            @error('password')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>

                        <div class="mb-2">
                            <label>Password Confirmation: </label>
                            <div class="mb-1">
                                <input type="text" id="password_confirmation" name="password_confirmation" placeholder="Users Password Confirmation" class="form-control" />
                            </div>

                            @error('password_confirmation')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- END: Modal Create Users --}}

    {{-- START: Modal Edit Users --}}
    <div class="modal fade text-start" id="modal-edit-users" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Edit Users</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="mb-2">
                            <label>Name: </label>
                            <div class="mb-1">
                                <input type="text" id="name" name="name" placeholder="Users Name" class="form-control" />
                            </div>

                            @error('name')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>

                        <div class="mb-2">
                            <label>Email: </label>
                            <div class="mb-1">
                                <input type="email" id="email" name="email" placeholder="example@gmail.com" class="form-control" />
                            </div>

                            @error('email')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>

                        <div class="mb-2">
                            <label>Roles: </label>
                            <div class="mb-1">
                                <select class="select2 form-select" name="roles" style="width: 100%;">
                                    <option></option>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->name }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>

                                @error('roles')
                                    <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-2">
                            <label>Password: </label>
                            <div class="mb-1">
                                <input type="text" id="password" name="password" placeholder="Users Password" class="form-control" />
                            </div>

                            @error('password')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>

                        <div class="mb-2">
                            <label>Password Confirmation: </label>
                            <div class="mb-1">
                                <input type="text" id="password_confirmation" name="password_confirmation" placeholder="Users Password Confirmation" class="form-control" />
                            </div>

                            @error('password_confirmation')
                                <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- END: Modal Edit Users --}}

    <form action="{{ route('admin.users.massDestroy') }}" id="deleteSelectedForm" method="POST" class="d-none">
        @csrf
        @method('DELETE')
        <input type="text" name="ids" id="ids">
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

    <script src="{{ asset('js/datatables/delete-button-init.js') }}"></script>
    <script src="{{ asset('js/datatables/bulk-delete.js') }}"></script>

    <script>
        $(document).ready(function() {
            const exportOption = [1, 2];

            const buttons = [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                },
                {
                    text: feather.icons['plus'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Add New Record',
                    className: 'create-new btn btn-primary me-2',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#modal-create-users'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    },
                },
                {
                    text: feather.icons['trash'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Bulk Delete',
                    className: 'create-new btn btn-danger',
                    action: function() {
                        startBulkDelete('{{ csrf_token() }}', '{{ route('admin.users.massDestroy') }}', '#datatable-users');
                    }
                }
            ];

            const datatable = $('#datatable-users').DataTable({
                processing: true,
                serverSide: true,
                search: {
                    return: true,
                },
                language: {
                    processing: 'Loading...'
                },
                ajax: '{!! route('admin.users.index') !!}',
                lengthMenu: [
                    [10, 50, 100, 500, 1000, -1],
                    [10, 50, 100, 500, 1000, 'All']
                ],
                columns: [{
                    defaultContent: ''
                }, {
                    data: 'name',
                    name: 'name'
                }, {
                    data: 'email',
                    name: 'email'
                }, {
                    data: 'roles',
                    name: 'roles'
                }, {
                    data: 'created_at',
                    name: 'created_at'
                }, {
                    data: 'options',
                    name: 'options'
                }],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: buttons,
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                columnDefs: [{
                    targets: 0,
                    orderable: false,
                    responsivePriority: 3,
                    render: function(data, type, full, meta) {
                        let id = full.id;

                        return (
                            '<div class="form-check"> <input class="form-check-input dt-checkboxes" type="checkbox" value="" id="checkbox-' +
                            id +
                            '" /><label class="form-check-label" for="checkbox' +
                            id +
                            '"></label></div>'
                        );
                    },
                    checkboxes: {
                        selectAllRender: '<div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="checkboxSelectAll" /><label class="form-check-label" for="checkboxSelectAll"></label></div>'
                    }
                }, ],
                order: [],
            });

            $('div.head-label').html('<h6 class="mb-0">DataTable Users</h6>');

            datatable.on('draw', () => {
                deleteButtonInit('{{ csrf_token() }}', '#datatable-users');
            });

            $('.select2').select2({
                placeholder: 'Select Roles',
                width: '100%',
                allowClear: false,
            });

            $(document).on('click', '.edit-user', function() {
                let id = $(this).data('id');
                let name = $(this).data('name');
                let email = $(this).data('email');
                let previous_roles = $(this).data('roles');

                previous_roles = JSON.parse(previous_roles); // 9q3093019302

                console.log(previous_roles);

                let modal_edit_users = $('#modal-edit-users');
                let input_name = modal_edit_users.find('input[name="name"]');
                let input_email = modal_edit_users.find('input[name="email"]');
                let select_roles = modal_edit_users.find('select[name="roles"]');
                let form_edit = modal_edit_users.find('form');

                let data_roles = {!! json_encode($roles) !!};

                select_roles.empty();

                $.each(data_roles, function(key, value) {
                    if (previous_roles == value.id) {
                        let option = new Option(value.name, value.id, true, true);
                        select_roles.append(option).trigger('change');
                    } else {
                        let option = new Option(value.name, value.id);
                        select_roles.append(option);
                    }
                });

                input_name.val(name);
                input_email.val(email);

                form_edit.attr('action', `{{ route('admin.users.update', ':id') }}`.replace(':id', id));
            });
        });
    </script>
@endpush
