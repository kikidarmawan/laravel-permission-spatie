<div class="d-flex flex-column flex-md-row justify-content-center" style="gap: 0.5rem">
    @can('users-update')
        <a href="#"
            data-bs-toggle="modal"
            data-bs-target="#modal-edit-users"
            data-id="{{ $model->id }}"
            data-name="{{ $model->name }}"
            data-email="{{ $model->email }}"
            data-roles="{{ $model->roles->pluck('id') }}"
            class="btn btn-sm btn-secondary edit-user">Edit</a>
    @endcan
    @can('users-delete')
        <span class="btn btn-sm btn-danger" id="deleteButton" data-model-id="{{ $model->id }}">Delete</span>
    @endcan
</div>
