@foreach ($model->roles as $role)
    <span class="badge bg-secondary">{{ $role->name }}</span>
    <span>{{ $loop->last ? '' : '|' }}</span>
@endforeach
