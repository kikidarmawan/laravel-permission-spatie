<div class="d-flex flex-column flex-md-row justify-content-center" style="gap: 0.5rem">
    @can('permissions-update')
        <a href="#"
            data-bs-toggle="modal"
            data-bs-target="#modal-edit-permissions"
            data-id="{{ $model->id }}"
            data-name="{{ $model->name }}"
            class="btn btn-sm btn-secondary edit-permission">Edit</a>
    @endcan
    @can('permissions-delete')
        <span class="btn btn-sm btn-danger" id="deleteButton" data-model-id="{{ $model->id }}">Delete</span>
    @endcan
</div>
