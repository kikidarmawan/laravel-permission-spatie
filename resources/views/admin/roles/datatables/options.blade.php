<div class="d-flex flex-column flex-md-row justify-content-center" style="gap: 0.5rem">
    @can('roles-update')
        <a href="#"
            data-bs-toggle="modal"
            data-bs-target="#modal-edit-roles"
            data-id="{{ $model->id }}"
            data-name="{{ $model->name }}"
            data-permission="{{ $model->permissions }}"
            class="btn btn-sm btn-secondary edit-role">Edit</a>
    @endcan
    @can('roles-delete')
        <span class="btn btn-sm btn-danger" id="deleteButton" data-model-id="{{ $model->id }}">Delete</span>
    @endcan
</div>
