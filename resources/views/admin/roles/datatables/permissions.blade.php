@if ($model->name == 'super admin')
    <span class="badge bg-success text-bold">ALL ACCESS</span>
@else
    @foreach ($model->permissions as $permission)
        <span class="badge bg-info">{{ $permission->name }}</span>
    @endforeach
@endif
