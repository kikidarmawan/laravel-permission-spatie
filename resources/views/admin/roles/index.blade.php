@extends('layouts.main', [
    'title' => 'Roles',
])

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">

    <style>
        .select2-search__field::placeholder {
            color: #6e6b7b;
            opacity: 0.6;
            padding: 0 0.5rem;
        }
    </style>
@endpush

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Dashboard</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Dashboard
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="">Roles</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            {{--  --}}
        </div>
    </div>
    <div class="content-body">
        <section id="basic-example">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <table class="datatables-basic table" id="datatable-roles">
                            <thead>
                                <tr>
                                    <th style="cursor: pointer" id="selector">
                                        <input type="checkbox" class="w-100" style="cursor: pointer">
                                        <span style="display: none;">Selector</span>
                                    </th>
                                    <th>Name</th>
                                    <th>Guard Name</th>
                                    <th>Permissions</th>
                                    <th>Created At</td>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- START: Modal Create Roles --}}
    <div class="modal fade text-start" id="modal-create-roles" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Create Roles</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('admin.roles.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-2">
                            <label>Role Name: </label>
                            <div class="mb-1">
                                <input type="text" id="name" name="name" placeholder="Role Name" class="form-control" />
                            </div>
                        </div>

                        <div class="mb-2">
                            <label>Permissions: </label>
                            <div class="mb-1">
                                <select class="select2 form-select" multiple="multiple" name="permissions[]" style="width: 100%;">
                                    @foreach ($permissions as $key => $permission)
                                        <optgroup label="{{ Str::upper($key) }}">
                                            @foreach ($permission as $item)
                                                <option value="{{ $item->name }}">{{ $item->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>

                                @error('permissions')
                                    <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- END: Modal Create Roles --}}

    {{-- START: Modal Edit Roles --}}
    <div class="modal fade text-start" id="modal-edit-roles" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Edit Roles</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="mb-2">
                            <label>Role Name: </label>
                            <div class="mb-1">
                                <input type="text" id="name" name="name" placeholder="Role Name" class="form-control" />

                                @error('name')
                                    <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-2">
                            <label>Permissions: </label>
                            <div class="mb-1">
                                <select class="select2 form-control" multiple="multiple" name="permissions[]" style="width: 100%;">
                                </select>

                                @error('permissions')
                                    <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- END: Modal Edit roles --}}

    <form action="{{ route('admin.roles.massDestroy') }}" id="deleteSelectedForm" method="POST" class="d-none">
        @csrf
        @method('DELETE')
        <input type="text" name="ids" id="ids">
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

    <script src="{{ asset('js/datatables/delete-button-init.js') }}"></script>
    <script src="{{ asset('js/datatables/bulk-delete.js') }}"></script>

    <script>
        $(document).ready(function() {
            const exportOption = [1, 2];

            const buttons = [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3]
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                },
                {
                    text: feather.icons['plus'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Add New Record',
                    className: 'create-new btn btn-primary me-2',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#modal-create-roles'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    },
                },
                {
                    text: feather.icons['trash'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Bulk Delete',
                    className: 'create-new btn btn-danger',
                    action: function() {
                        startBulkDelete('{{ csrf_token() }}', '{{ route('admin.roles.massDestroy') }}', '#datatable-roles');
                    }
                }
            ];

            const datatable = $('#datatable-roles').DataTable({
                processing: true,
                serverSide: true,
                search: {
                    return: true,
                },
                language: {
                    processing: 'Loading...'
                },
                ajax: '{!! route('admin.roles.index') !!}',
                lengthMenu: [
                    [10, 50, 100, 500, 1000, -1],
                    [10, 50, 100, 500, 1000, 'All']
                ],
                columns: [{
                    defaultContent: ''
                }, {
                    data: 'name',
                    name: 'name'
                }, {
                    data: 'guard_name',
                    name: 'guard_name'
                }, {
                    data: 'permissions',
                    name: 'permissions'
                }, {
                    data: 'created_at',
                    name: 'created_at'
                }, {
                    data: 'options',
                    name: 'options'
                }],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: buttons,
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                columnDefs: [{
                    targets: 0,
                    orderable: false,
                    responsivePriority: 3,
                    render: function(data, type, full, meta) {
                        let id = full.id;

                        return (
                            '<div class="form-check"> <input class="form-check-input dt-checkboxes" type="checkbox" value="" id="checkbox-' +
                            id +
                            '" /><label class="form-check-label" for="checkbox' +
                            id +
                            '"></label></div>'
                        );
                    },
                    checkboxes: {
                        selectAllRender: '<div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="checkboxSelectAll" /><label class="form-check-label" for="checkboxSelectAll"></label></div>'
                    }
                }, ],
                order: [],
            });

            $('div.head-label').html('<h6 class="mb-0">DataTable Roles</h6>');

            datatable.on('draw', () => {
                deleteButtonInit('{{ csrf_token() }}', '#datatable-roles');
            });

            $('.select2').select2({
                placeholder: 'Select Roles',
                width: '100%',
                selectOnClose: false,
            });

            $(document).on('click', '.edit-role', function() {
                let id = $(this).data('id');
                let name = $(this).data('name');
                let previous_permission = $(this).data('permission');

                console.log(previous_permission);

                let modal_edit_roles = $('#modal-edit-roles');
                let input_name = modal_edit_roles.find('input[name="name"]');
                let select_permission = modal_edit_roles.find('select[name="permissions[]"]');
                let form_edit = modal_edit_roles.find('form');

                let data_permission = {!! json_encode($permissions) !!};
                select_permission.empty();

                for (const groupName in data_permission) {
                    if (data_permission.hasOwnProperty(groupName)) {
                        const group = data_permission[groupName];
                        const optgroup = $('<optgroup>').attr('label', groupName.toUpperCase());

                        group.forEach(permission => {
                            const option = $('<option>').val(permission.name).text(permission.name);
                            optgroup.append(option);
                        });

                        select_permission.append(optgroup);
                    }
                }

                previous_permission.forEach(function(prevPermission) {
                    select_permission.find(`option[value="${prevPermission.name}"]`).prop('selected', true);
                });

                select_permission.select2();

                input_name.val(name);

                form_edit.attr('action', `{{ route('admin.roles.update', ':id') }}`.replace(':id', id));
            });
        });
    </script>
@endpush
